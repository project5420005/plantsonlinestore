package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.dao.UserDao;
import com.model.User;

@RestController
public class UserController {

    @Autowired
    UserDao userDao;

    @GetMapping("getUsers")
    public List<User> getUsers() {
        return userDao.getUsers();
    }

    @GetMapping("getUserById/{userId}")
    public User getUserById(@PathVariable Long userId) {
        return userDao.getUserById(userId);
    }

    @GetMapping("getUserByName/{userName}")
    public User getUserByName(@PathVariable String userName) {
        return userDao.getUserByName(userName);
    }

    @PostMapping("loginUser")
    public ResponseEntity<String> loginUser(@RequestParam String emailId, @RequestParam String password) {
        try {
            User loggedInUser = userDao.loginUser(emailId, password);
            return ResponseEntity.ok("Login successful. User ID: " + loggedInUser);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Invalid email or password");
        }
    }

    @PostMapping("addUser")
    public ResponseEntity<String> addUser(@RequestBody User user) {
        try {
            String result = userDao.addUser(user);
            return ResponseEntity.ok(result);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Error adding user: " + e.getMessage());
        }
    }

    @PutMapping("updateUser")
    public ResponseEntity<User> updateUser(@RequestBody User user) {
        try {
            User updatedUser = userDao.updateUser(user);
            return ResponseEntity.ok(updatedUser);
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("deleteUserById/{userId}")
    public ResponseEntity<String> deleteUserById(@PathVariable Long userId) {
        try {
            userDao.deleteUserById(userId);
            return ResponseEntity.ok("User with UserId: " + userId + " deleted successfully");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Error deleting user: " + e.getMessage());
        }
    }
}

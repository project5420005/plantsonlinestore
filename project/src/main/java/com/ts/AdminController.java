package com.ts;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.NurseryRepository;
import com.model.Nursery;

@RestController
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private NurseryRepository nurseryRepository;

    @GetMapping("/nurseries")
    public List<Nursery> getNurseries() {
        return nurseryRepository.findAll();
    }
}

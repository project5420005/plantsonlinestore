package com.ts;

import java.util.List;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.NurseryDao;
import com.model.Nursery;
import com.model.Owner;

@RestController
@RequestMapping
public class NurseryController {

	@Autowired
	private NurseryDao nurseryDao;

	@GetMapping("getAllNurseries")
	public List<Nursery> getAllNurseries() {
		return nurseryDao.getAllNurseries();
	}

	@GetMapping("getNurseryById/{nurseryId}")
	public Nursery getNurseryById(@PathVariable int nurseryId) {
		return nurseryDao.getNurseryById(nurseryId);
	}

	@GetMapping("getNurseryByName/{nurseryName}")
	public Nursery getNurseryByName(@PathVariable String nurseryName) {
		return nurseryDao.getNurseryByName(nurseryName);
	}

	@GetMapping("nurseryLogin/{emailId}/{password}")
	public ResponseEntity<String> nurseryLogin(@PathVariable String emailId, @PathVariable String password) {
		Nursery loggedInNursery = nurseryDao.nurseryLogin(emailId, password);
		if (loggedInNursery != null) {
			return ResponseEntity.ok("Login successful. User ID: " + loggedInNursery.getNurseryId());
		} else {
			return ResponseEntity.status(HttpStatus.SC_UNAUTHORIZED).body("Invalid email or password");
		}
	}

	@PostMapping("addNursery")
	public Nursery addNursery(@RequestBody Nursery nursery) {
		return nurseryDao.addNursery(nursery);
	}

	@PutMapping("updateNursery")
	public Nursery updateNursery(@RequestBody Nursery nursery) {
		return nurseryDao.updateNursery(nursery);
	}

	@DeleteMapping("deleteNurseryById/{nurseryId}")
	public void deleteNursery(@PathVariable int nurseryId) {
		nurseryDao.deleteNursery(nurseryId);
	}
}

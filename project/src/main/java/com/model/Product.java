package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity

public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long pId;
	private String name;
	private String type;
	@Lob
	private byte[] image;

	@ManyToOne
	@JoinColumn(name = "nursery_id")
	private Nursery nursery;

	public Product() {
	}

	public Product(Long pId, String name, String type, byte[] image) {
		this.pId = pId;
		this.name = name;
		this.type = type;
		this.image = image;
	}

	public Long getPId() {
		return pId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

}
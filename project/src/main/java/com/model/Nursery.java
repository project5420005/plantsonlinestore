package com.model;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Nursery {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int nurseryId;

    private String nurseryName;
    private String location;
    private String phoneNumber;

    @Column(unique = true)
    private String emailId;
    private String password;

    @ManyToOne
    @JoinColumn(name = "ownerId", nullable = false)
    private Owner owner;

    @ManyToOne
    @JoinColumn(name = "productId", nullable = false)
    private Product product;

    @OneToMany(mappedBy = "nursery")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<Product> products;

    public Nursery() {
    }

    public Nursery(String nurseryName, String location, String phoneNumber, String emailId, String password,
            List<Product> products, Owner owner, Product product) {

        this.nurseryName = nurseryName;
        this.location = location;
        this.phoneNumber = phoneNumber;
        this.emailId = emailId;
        this.password = password;
        this.products = products;
        this.owner = owner;
        this.product = product;
    }

    public int getNurseryId() {
        return nurseryId;
    }

    public String getNurseryName() {
        return nurseryName;
    }

    public void setNurseryName(String nurseryName) {
        this.nurseryName = nurseryName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonProperty("ownerId")
    public int getOwnerId() {
        return (int) ((this.owner != null) ? this.owner.getOwnerId() : 0);
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

	public void setOwner(int owner2) {
		// TODO Auto-generated method stub
		
	}
}

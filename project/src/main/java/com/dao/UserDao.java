package com.dao;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.model.Owner;
import com.model.User;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class UserDao {

	@Autowired
	UserRepository userRepository;

	@Autowired
	private JavaMailSender mailSender;

	private static final String ACCOUNT_SID = "ACa83e3dbcd8ca6019893558f571b895be";
	private static final String AUTH_TOKEN = "1d27211ce3a06c01bbdf18704f443554";
	private static final String TWILIO_PHONE_NUMBER = "+16073036141";
	private String otp;

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	static {
		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
	}

	public List<User> getUsers() {
		return userRepository.findAll();
	}

	public User getUserById(Long userId) {
		return userRepository.findById(userId).orElse(null);
	}

	public User getUserByName(String userName) {
		return userRepository.findByName(userName);
	}

	public User loginUser(String emailId, String password) {
		User user = userRepository.findByEmailId(emailId);

		if (user != null) {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			if (passwordEncoder.matches(password, user.getPassword())) {
				return user;
			}
		}

		return null;
	}


	@PostMapping("/addUser")
	public String addUser(@RequestBody User user) {
		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
		String encryptedPwd = bcrypt.encode(user.getPassword());
		user.setPassword(encryptedPwd);
		User savedUser = userRepository.save(user);
		String otp = generateOtp();
		setOtp(otp);

		sendWelcomeEmail(savedUser);
		sendOtpSMS(savedUser);

		return savedUser.getEmailId() + " registered successfully";
	}

	private String generateOtp() {
		Random random = new Random();
		int otp = 100000 + random.nextInt(900000);
		return String.valueOf(otp);
	}

	private void sendWelcomeEmail(User user) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(user.getEmailId());
		message.setSubject("Welcome to RMD website");
		message.setText("Dear " + user.getUsername() + ",\n\n" + "Thank you for registering ");

		mailSender.send(message);
	}

	private void sendOtpSMS(User user) {
		Message message = Message.creator(new PhoneNumber(user.getPhoneNumber()), new PhoneNumber(TWILIO_PHONE_NUMBER),
				"Your OTP for registration is: " + getOtp()).create();
		System.out.println("SMS Sent SID: " + message.getSid());
	}

	 public User updateUser(User updatedUser) {
	        // Retrieve the existing user
	        User existingUser = userRepository.findById(updatedUser.getUserId()).orElse(null);

	        if (existingUser != null) {
	            // Update other fields as needed
	            existingUser.setUsername(updatedUser.getUsername());
	            existingUser.setEmailId(updatedUser.getEmailId());
	            existingUser.setPhoneNumber(updatedUser.getPhoneNumber());

	            // Check if the password is updated
	            if (!updatedUser.getPassword().equals(existingUser.getPassword())) {
	                // Password has been changed, re-encrypt it
	                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	                String encryptedPassword = passwordEncoder.encode(updatedUser.getPassword());
	                existingUser.setPassword(encryptedPassword);
	            }

	            // Save the updated user
	            userRepository.save(existingUser);

	            return existingUser;
	        }

	        return null; // Handle error if user not found
	    }

	public void deleteUserById(Long userId) {
		userRepository.deleteById(userId);
	}

}

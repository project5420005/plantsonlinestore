package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Plants;

@Service
public class PlantsDao {

	@Autowired
	private PlantsRepository plantsRepository;

	public List<Plants> getAllPlants() {
		return plantsRepository.findAll();
	}

	public Plants getPlantById(int plantId) {
		return plantsRepository.findByPlantId(plantId);
	}

	public List<Plants> getPlantByName(String plantName) {
		return plantsRepository.findByPlantName(plantName);
	}

	public List<Plants> getPlantByPrice(Long price) {
		return plantsRepository.findByPrice(price);
	}

	public List<Plants> getPlantByType(String type) {
		return plantsRepository.findByType(type);
	}

	public Plants addPlant(Plants plant) {
		return plantsRepository.save(plant);
	}

	public Plants updatePlant(Plants plant) {
		return plantsRepository.save(plant);
	}

	public void deletePlant(int plantId) {
		plantsRepository.deleteById(plantId);
	}
}

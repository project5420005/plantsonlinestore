package com.dao;

import com.model.Plants;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlantsRepository extends JpaRepository<Plants, Integer> {
    Plants findByPlantId(int plantId);
    List<Plants> findByPlantName(String plantName);
    List<Plants> findByPrice(Long price);
    List<Plants> findByType(String type);
}

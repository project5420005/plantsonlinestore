package com.dao;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.model.Owner;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class OwnerDao {

    @Autowired
    OwnerRepository ownerRepository;

    @Autowired
    private JavaMailSender mailSender;

    // Twilio credentials
    private static final String ACCOUNT_SID = "ACa83e3dbcd8ca6019893558f571b895be";
    private static final String AUTH_TOKEN = "1d27211ce3a06c01bbdf18704f443554";
    private static final String TWILIO_PHONE_NUMBER = "+16073036141";

    private String otp;
    
    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
    static {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    }

    @GetMapping
    public List<Owner> getOwners() {
        return ownerRepository.findAll();
    }

    public Owner getOwnerById(int ownerId) {
        return ownerRepository.findById(ownerId).orElse(null);
    }

    public Owner getOwnerByName(String ownerName) {
        return ownerRepository.findByUserName(ownerName);
    }

    public Employee employeeLogin(String emailId, String password) {
        return employeeRepository.employeeLogin(emailId, password);
    }

    @PostMapping
    public Employee addEmployee(Employee employee) {
        BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
        String encryptedPwd = bcrypt.encode(employee.getPassword());
        employee.setPassword(encryptedPwd);

        // Generate and save OTP
        String otp = generateOtp();
        setOtp(otp);

        // Save the employee
        Employee savedEmployee = employeeRepository.save(employee);

        // Send a welcome email
        sendWelcomeEmail(savedEmployee);

        // Send an SMS with the OTP to the mobile number
        sendOtpSMS(savedEmployee);

        return savedEmployee;
    }

    private String generateOtp() {
        // Generate a 6-digit OTP
        Random random = new Random();
        int otp = 100000 + random.nextInt(900000);
        return String.valueOf(otp);
    }

    private void sendWelcomeEmail(Employee employee) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(employee.getEmailId());
        message.setSubject("Welcome to our website");
        message.setText("Dear " + employee.getEmpName() + ",\n\n"
                + "Thank you for registering ");

        mailSender.send(message);
    }

    private void sendOtpSMS(Employee employee) {
        Message message = Message.creator(
                new PhoneNumber(employee.getMobile()),
                new PhoneNumber(TWILIO_PHONE_NUMBER),
                "Your OTP for registration is: " + getOtp()).create();

        System.out.println("SMS Sent SID: " + message.getSid());
    }

    public Employee updateEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    public void deleteEmployeeById(int employeeId) {
        employeeRepository.deleteById(employeeId);
    }
}
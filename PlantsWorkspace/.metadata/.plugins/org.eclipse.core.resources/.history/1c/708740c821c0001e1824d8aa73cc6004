package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name = "members")
public class Members {

    @Id
    @GeneratedValue
    @Column(name = "member_id")
    private Long memberId;

    @ManyToMany(mappedBy = "members")
    private List<Plants> plants = new ArrayList<>();
    
	private String memberName;
	private String country;
	private String mobileNumber;
	private String emailId;
	private String password;

	@ManyToMany(mappedBy = "members")
	private List<Pots> potList = new ArrayList<>();

	@ManyToMany(mappedBy = "members")
	private List<Plants> plantList = new ArrayList<>();

	@ManyToMany(mappedBy = "members")
	private List<Tools> toolList = new ArrayList<>();

	@ManyToMany
	@JoinTable(
	    name = "members_pesticides",
	    joinColumns = @JoinColumn(name = "member_id"),
	    inverseJoinColumns = @JoinColumn(name = "pesticide_id")
	)
	private List<Pesticides> pesticides = new ArrayList<>();


	@ManyToMany(mappedBy = "members")
	private List<Fertilizers> fertilizerList = new ArrayList<>();


	
	@ManyToMany(mappedBy = "members")
	private List<Soil> soilList = new ArrayList<>();

	@ManyToOne
	@JoinColumn(name = "potId")
	private Pots pot;

	@ManyToOne
	@JoinColumn(name = "plantsId")
	private Plants plant;

	@ManyToOne
	@JoinColumn(name = "toolId")
	private Tools tool;

	@ManyToOne
	@JoinColumn(name = "pesticideId")
	private Pesticides pesticide;

	@ManyToOne
	@JoinColumn(name = "fertilizerId")
	private Fertilizers fertilizer;

	@ManyToOne
	@JoinColumn(name = "soilId")
	private Soil soil;

	public Members() {
	}

	public Members(String memberName, String country, String mobileNumber, String emailId, String password) {
		this.memberName = memberName;
		this.country = country;
		this.emailId = emailId;
		this.password = password;
		this.mobileNumber = mobileNumber;
	}

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Pots> getPotList() {
		return potList;
	}

	public void setPotList(List<Pots> potList) {
		this.potList = potList;
	}

	public List<Plants> getPlantList() {
		return plantList;
	}

	public void setPlantList(List<Plants> plantList) {
		this.plantList = plantList;
	}

	public List<Tools> getToolList() {
		return toolList;
	}

	public void setToolList(List<Tools> toolList) {
		this.toolList = toolList;
	}

	public List<Fertilizers> getFertilizerList() {
		return fertilizerList;
	}

	public void setFertilizerList(List<Fertilizers> fertilizerList) {
		this.fertilizerList = fertilizerList;
	}

	public void setSoilList(List<Soil> soilList) {
		this.soilList = soilList;
	}
	public Soil getSoil() {
		return soil;
	}

	public void setSoil(Soil soil) {
		this.soil = soil;
	}

	public Pots getPot() {
		return pot;
	}

	public void setPot(Pots pot) {
		this.pot = pot;
	}

	public Plants getPlant() {
		return plant;
	}

	public void setPlant(Plants plant) {
		this.plant = plant;
	}

	public Tools getTool() {
		return tool;
	}

	public void setTool(Tools tool) {
		this.tool = tool;
	}

	public Pesticides getPesticide() {
		return pesticide;
	}

	public void setPesticide(Pesticides pesticide) {
		this.pesticide = pesticide;
	}

	public Fertilizers getFertilizer() {
		return fertilizer;
	}

	public void setFertilizer(Fertilizers fertilizer) {
		this.fertilizer = fertilizer;
	}

}

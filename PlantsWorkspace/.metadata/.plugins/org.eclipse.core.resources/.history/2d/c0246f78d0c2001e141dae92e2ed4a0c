package com.dao;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.model.User;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class UserDao {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private JavaMailSender mailSender;

    // Twilio credentials
    private static final String ACCOUNT_SID = "ACa83e3dbcd8ca6019893558f571b895be";
    private static final String AUTH_TOKEN = "1d27211ce3a06c01bbdf18704f443554";
    private static final String TWILIO_PHONE_NUMBER = "+16073036141";

    private String otp;
    
    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
    static {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    }

    @GetMapping
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public User getUserById(Long userId) {
        return userRepository.findById(userId).orElse(null);
    }

    public User getUserByName(String userName) {
        return userRepository.findByName(userName);
    }

    @PostMapping
    public ResponseEntity<User> addUser(@RequestBody User user) {
        // Check if the email is already registered
        if (userRepository.findByEmailId(user.getEmailId()) != null) {
            return ResponseEntity.badRequest().body(null); // You can customize the response as needed
        }

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encryptedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encryptedPassword);

        // Generate and save OTP
        String otp = generateOtp();
        setOtp(otp);

        // Save the user
        User savedUser = userRepository.save(user);

        // Send a welcome email
        sendWelcomeEmail(savedUser);

        // Send an SMS with the OTP to the mobile number
        sendOtpSMS(savedUser);

        return ResponseEntity.ok(savedUser);
    }

    public User userLogin(String email_id, String password) {
        return userRepository.userLogin(email_id, password);
    }


    private String generateOtp() {
        // Generate a 6-digit OTP
        Random random = new Random();
        int otp = 100000 + random.nextInt(900000);
        return String.valueOf(otp);
    }

    private void sendWelcomeEmail(User user) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(user.getEmailId());
        message.setSubject("Welcome to RMD website");
        message.setText("Dear " + user.getUsername() + ",\n\n" + "Thank you for registering ");

        mailSender.send(message);
    }

    private void sendOtpSMS(User user) {
        Message message = Message.creator(new PhoneNumber(user.getPhoneNumber()),
                new PhoneNumber(TWILIO_PHONE_NUMBER), "Your OTP for registration is: " + getOtp()).create();

        System.out.println("SMS Sent SID: " + message.getSid());
    }

    public User updateUser(User user) {
        return userRepository.save(user);
    }

    public void deleteUserById(Long userId) {
        userRepository.deleteById(userId);
    }
}
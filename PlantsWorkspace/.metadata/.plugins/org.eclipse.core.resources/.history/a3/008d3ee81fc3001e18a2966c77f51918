package com.ts;

import java.util.List;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserDao;
import com.model.User;

@RestController

public class UserController {
	@Autowired
	UserDao userDao;

	@GetMapping("getUsers")
	public List<User> getUsers() {
		return userDao.getUsers();
	}

	@GetMapping("getUserById/{userId}")
	public User getUserById(@PathVariable Long userId) {
		return userDao.getUserById(userId);
	}

	@GetMapping("getUserByName/{userName}")
	public User getUserByName(@PathVariable String userName) {
		return userDao.getUserByName(userName);
	}

	@GetMapping("/userLogin/{emailId}/{password}")
	public ResponseEntity<User> userLogin(@PathVariable String emailId, @PathVariable String password) {
		User loggedInUser = userDao.userLogin(emailId, password);
		if (loggedInUser != null) {
			return ResponseEntity.ok(loggedInUser);
		} else {
			return ResponseEntity.status(HttpStatus.SC_UNAUTHORIZED).body(null);
		}
	}

	@PostMapping("addUser")
	public ResponseEntity<User> addUser(@RequestBody User user) {
		return userDao.addUser(user);
	}

	@PutMapping("updateUser")
	public User updateUser(@RequestBody User user) {
		return userDao.updateUser(user);
	}

	@DeleteMapping("deleteUserById/{userId}")
	public String deleteUserById(@PathVariable Long userId) {
		userDao.deleteUserById(userId);
		return "User With UserId:" + userId + " Deleted Successfully!!!";
	}
}

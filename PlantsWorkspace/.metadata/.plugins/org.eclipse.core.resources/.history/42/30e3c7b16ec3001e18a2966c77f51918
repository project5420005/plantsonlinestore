package com.dao;

import com.model.Plants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlantService {

    @Autowired
    private PlantRepository plantRepository;

    public List<Plants> getAllPlants() {
        return plantRepository.findAll();
    }

    public Plants getPlantById(int plantId) {
        return plantRepository.findByPlantId(plantId);
    }

    public List<Plants> getPlantByName(String plantName) {
        return plantRepository.findByPlantName(plantName);
    }

    public List<Plants> getPlantByPrice(Long price) {
        return plantRepository.findByPrice(price);
    }

    public List<Plants> getPlantByType(String type) {
        return plantRepository.findByType(type);
    }

    public Plants addPlant(Plants plant) {
        return plantRepository.save(plant);
    }

    public Plants updatePlant(Plants plant) {
        return plantRepository.save(plant);
    }

    public void deletePlant(int plantId) {
        plantRepository.deleteById(plantId);
    }
}

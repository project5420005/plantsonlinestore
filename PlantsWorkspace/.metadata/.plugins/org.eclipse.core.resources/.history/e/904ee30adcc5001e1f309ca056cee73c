package com.dao;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.model.Nursery;
import com.model.Owner;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class NurseryDao {

	@Autowired
	private NurseryRepository nurseryRepository;

	@Autowired
	private JavaMailSender mailSender;

	// Twilio credentials
	private static final String ACCOUNT_SID = "ACa83e3dbcd8ca6019893558f571b895be";
	private static final String AUTH_TOKEN = "1d27211ce3a06c01bbdf18704f443554";
	private static final String TWILIO_PHONE_NUMBER = "+16073036141";

	private String otp;

	private Object ownerId;

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	static {
		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
	}

	@GetMapping

	public List<Nursery> getAllNurseries() {
		return nurseryRepository.findAll();
	}

	@GetMapping
	public Nursery getNurseryById(int nurseryId) {
		return nurseryRepository.findById(nurseryId).orElse(null);
	}

	public Nursery getNurseryByName(String nurseryName) {
		return nurseryRepository.findNurseryByName(nurseryName);
	}

	@GetMapping
	public Nursery nurseryLogin(String emailId, String password) {
		Nursery nursery = nurseryRepository.findByEmailId(emailId);

		if (nursery != null) {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			if (passwordEncoder.matches(password, nursery.getPassword())) {
				return nursery;
			}
		}

		return null;
	}

	@PostMapping
	public Nursery addNursery(@RequestBody Nursery nursery) {
	    BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
	    String encryptedPwd = bcrypt.encode(nursery.getPassword());
	    nursery.setPassword(encryptedPwd);
	    Nursery savedNursery = nurseryRepository.save(nursery);

	    String generatedOtp = generateOtp();
	    setOtp(generatedOtp); // Set the generated OTP

	    sendWelcomeEmail(savedNursery);
	    sendOtpSMS(savedNursery);

	    return savedNursery;
	}

	private String generateOtp() {
		// Generate a 6-digit OTP
		Random random = new Random();
		int otp = 100000 + random.nextInt(900000);
		return String.valueOf(otp);
	}

	private void sendWelcomeEmail(Nursery nursery) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(nursery.getEmailId());
		message.setSubject("Welcome to our website");
		message.setText("Dear " + nursery.getNurseryName() + ",\n\n" + "Thank you for registering ");

		mailSender.send(message);
	}

	private void sendOtpSMS(Nursery nursery) {
		Message message = Message.creator(new PhoneNumber(nursery.getPhoneNumber()), new PhoneNumber(TWILIO_PHONE_NUMBER),
				"Your OTP for registration is: " + getOtp()).create();

		System.out.println("SMS Sent SID: " + message.getSid());
	}

	@PutMapping
	public Nursery updateNursery(Nursery nursery) {
		return nurseryRepository.save(nursery);
	}

	@DeleteMapping
	public void deleteNursery(int nurseryId) {
		nurseryRepository.deleteById(nurseryId);
	}

}

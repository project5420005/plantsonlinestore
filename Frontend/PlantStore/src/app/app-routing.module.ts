import { Component, NgModule } from '@angular/core';
 // Import the new component
import { RouterModule, Routes } from '@angular/router';
import { OwnersHeaderComponent } from './owners-header/owners-header.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
const routes: Routes = [
  
  // Add other routes as needed
  {path:'', component:HomeComponent},
  {path:'about',component:AboutComponent}

  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

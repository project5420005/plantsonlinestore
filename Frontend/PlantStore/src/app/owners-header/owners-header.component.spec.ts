import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnersHeaderComponent } from './owners-header.component';

describe('OwnersHeaderComponent', () => {
  let component: OwnersHeaderComponent;
  let fixture: ComponentFixture<OwnersHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OwnersHeaderComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(OwnersHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
